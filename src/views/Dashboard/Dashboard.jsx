import React from 'react';
import { Route, Routes } from 'react-router-dom';
import NavbarWithDrawer
  from '../../components/NavbarWithDrawer/NavbarWithDrawer';
import Profile from '../../components/Profile/Profile';
import SubmitWorkItemForm
  from '../../components/SubmitWorkItemForm/SubmitWorkItemForm';
import WorkItemDetails from '../../components/WorkItemDetails/WorkItemDetails';
import CreateTeam from '../../components/CreateTeam/CreateTeam';
import MyTeams from '../../components/MyTeams/MyTeams';
import CreateReviewRequest
  from '../../components/CreateReviewRequest/CreateReviewRequest';
import ViewWorkItems from '../../components/ViewWorkItems/ViewWorkItems';
import ViewReviewRequests from '../../components/ViewReviewRequests/ViewReviewRequests';
import DashboardHome from '../../components/DashboardHome/DashboardHome';
import ViewUserProfile from '../../components/ViewUserProfile/ViewUserProfile';
import ViewAllNotifications from '../../components/ViewAllNotifications/ViewAllNotifications';

const Dashboard = () => {
  return (
    <NavbarWithDrawer>
      <Routes>
        <Route path="/" element={<DashboardHome />} />
        <Route path="submit-item" element={<SubmitWorkItemForm />} />
        <Route path="work-item/:id" element={<WorkItemDetails />} />
        <Route
          path="work-item/:id/request-review"
          element={<CreateReviewRequest />}
        />
        <Route path="profile" element={<Profile />} />
        <Route path="create-team" element={<CreateTeam />} />
        <Route path="my-teams" element={<MyTeams />} />
        <Route path="view-work-items" element={<ViewWorkItems />} />
        <Route path="view-review-requests" element={<ViewReviewRequests />} />
        <Route path="users/:username" element={<ViewUserProfile />} />
        <Route path="notifications" element={<ViewAllNotifications />} />
      </Routes>
    </NavbarWithDrawer>
  );
};

export default Dashboard;
