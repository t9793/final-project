import React from 'react';
import List from '@mui/material/List';
import HistoryListItem from '../HistoryListItem/HistoryListItem';


const WorkItemHistory = ({ history, workItemAuthor }) => {
  return (
    <List sx={{
      mt: 2,
      width: '100%' }}>
      {Object.entries(history)
          .sort(
              // eslint-disable-next-line no-unused-vars
              ([_, valA], [__, valB]) => valB.createdOn - valA.createdOn,
          )
          .map(([key, value]) => (
            <HistoryListItem key={key} historyItem={value} workItemAuthor={workItemAuthor} />
          ))}
    </List>
  );
};

export default WorkItemHistory;
