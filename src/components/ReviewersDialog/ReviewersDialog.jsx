import React, { useEffect, useState } from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import {
  FormControlLabel,
  FormGroup,
  Checkbox,
  Box,
  FormControl,
  DialogActions,
  Button,
} from '@mui/material';

const ReviewersDialog = ({
  open,
  onClose,
  selectedValues,
  optionsArray,
}) => {
  const [currentSelection,
    setCurrentSelection] = useState({});

  useEffect(() => {
    setCurrentSelection(selectedValues);
  }, [selectedValues]);

  const handleClose = () => {
    onClose(currentSelection);
  };

  const handleChange = (event) => {
    setCurrentSelection((prev) => ({
      ...prev,
      [event.target.name]: event.target.checked,
    }));
  };

  return (
    <Box sx={{ display: 'flex', m: 5 }}>
      <Dialog onClose={handleClose} open={open}>
        <DialogTitle>Select Reviewers</DialogTitle>
        <FormControl
          sx={{ m: 3, p: 1 }}
          component="fieldset"
          variant="standard"
        >
          <FormGroup>
            {optionsArray.map((teamMember) =>
              (<FormControlLabel key={teamMember}
                control={
                  <Checkbox
                    checked={!!currentSelection[teamMember]}
                    onChange={handleChange}
                    name={teamMember}
                  />
                }
                label={teamMember}
              />))}
          </FormGroup>
        </FormControl>
        <DialogActions>
          <Button onClick={handleClose}>Submit</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

export default ReviewersDialog;
