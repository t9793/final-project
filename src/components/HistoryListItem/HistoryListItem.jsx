import React, { useState, useEffect } from 'react';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { Chip } from '@mui/material';
import Moment from 'react-moment';
import { getUserByUsername } from '../../services/users.services';

const HistoryListItem = ({ historyItem, workItemAuthor }) => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    getUserByUsername(historyItem.author)
        .then((snapshot) => {
          if (!snapshot.exists()) return;
          setUser(snapshot.val());
        })
        .catch(console.error);
  }, [historyItem]);

  return (
    <>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Author" src={user ? user.avatarUrl : ''} />
        </ListItemAvatar>
        {historyItem.newStatus === '' ? null : (
          <Chip
            label={historyItem.newStatus}
            variant="outlined"
            color="primary"
            sx={{ position: 'absolute', right: '1rem', top: '1rem' }}
          />
        )}
        <ListItemText
          primary={
            <React.Fragment>
              <Typography
                sx={{ display: 'inline' }}
                component="span"
                variant="h6"
                color="text.primary"
              >
                {historyItem.author + (historyItem.author === workItemAuthor ? ' (author)' : '')}
              </Typography>
              <Typography
                sx={{ display: 'inline', ml: 1 }}
                component="span"
                variant="body2"
                color="gray"
              >
                <Moment fromNow>{historyItem.createdOn}</Moment>
              </Typography>
            </React.Fragment>
          }
          secondary={
            <React.Fragment>
              <Typography
                sx={{ display: 'block' }}
                component="span"
                variant="body2"
                color="text.primary"
              >
                {historyItem.comment}
              </Typography>
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
    </>
  );
};

export default HistoryListItem;
