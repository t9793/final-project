import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getUserByUsername } from '../../services/users.services';
import { Typography, Grid, Paper, Box, Avatar } from '@mui/material';
import Moment from 'react-moment';

export default function ViewUserProfile() {
  const { username } = useParams();
  const [user, setUser] = useState(null);

  useEffect(() => {
    getUserByUsername(username)
        .then((snapshot) => {
          if (!snapshot.exists()) return;
          setUser(snapshot.val());
        })
        .catch(console.error);
  }, [username]);

  return (
    user !== null ?
    <Box sx={{ flexGrow: 1, maxWidth: '80%', ml: 'auto', mr: 'auto' }}>
      <Typography variant="h4" component="h2" sx={{ textAlign: 'left', mb: 3 }}>
        {`${username}\'s profile`}
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Paper
            sx={{
              height: 300,
              position: 'relative',
            }}
          >
            <Box
              sx={{
                height: 300,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
                pl: 5,
                pr: 5,
              }}
            >
              <Avatar
                sx={{ width: 200, height: 200, mb: 4 }}
                src={user.avatarUrl}
              />
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={8} sx={{ height: 600 }}>
          <Paper
            sx={{
              height: '50%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'column',
              position: 'relative',
              pl: 5,
              pr: 5,
              mb: 2,
            }}
          >
            <Grid container rowSpacing={3} columnSpacing={3} sx={{ mb: 3 }}>
              <Grid item xs={12}>
                <Typography
                  variant="h5"
                  component="h3"
                  sx={{
                    textAlign: 'center',
                  }}
                >
                    Details
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  label="First Name"
                  component="div"
                >
                  First Name: {user.firstName}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  label="Last Name"
                  component="div"
                >
                  Last Name: {user.lastName}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  label="E-mail"
                  component="div"
                >
                  E-mail: {user.email}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  label="Phone"
                  component="div"
                >
                  Phone: {user.phone}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography
                  label="Last online"
                  component="div"
                >
                  {'Last login: '}
                  <Moment fromNow>
                    {user.lastLogIn}
                  </Moment>
                </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </Box> :
    null
  );
};
