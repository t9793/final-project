import React, { useEffect, useState } from 'react';
import {
  Tab,
  Box,
  List,
  ListItem,
  ListItemText,
  Typography,
  Chip,
  Button,
  Divider,
  Paper } from '@mui/material';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import Moment from 'react-moment';
import {
  getAllWorkItems,
  getWorkItemsByAuthor } from '../../services/work-items.services';
import { useNavigate } from 'react-router-dom';

const ViewWorkItems = () => {
  const { userData } = useContext(AppContext);
  const [items, setItems] = useState([]);
  const [teamItems, setTeamItems] = useState([]);
  const [value, setValue] = React.useState('0');
  const navigate = useNavigate();
  useEffect(() => {
    if (!userData) return;

    getWorkItemsByAuthor(userData.username)
        .then((res) => setItems(res.sort((a, b) => a.createdOn - b.createdOn)))
        .catch(console.error);

    getAllWorkItems()
        .then((res) => {
          const teamFiltered = res
              .filter((item) => Object.values(userData.teams).includes(item.teamName))
              .sort((a, b) => a.createdOn - b.createdOn);
          setTeamItems(teamFiltered);
        })
        .catch(console.error);
  }, [userData]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleDetailsClick = (id) => {
    navigate(`../work-item/${id}`);
  };

  return (
    <Box sx={{ width: '1180px', ml: 'auto', mr: 'auto' }}>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange}>
            <Tab label="My Work Items" value='0' />
            <Tab label="Work items by team" value='1' />
          </TabList>
        </Box>
        <TabPanel value='0' index={0}>
          <List>
            {
              items.length === 0 ?
              'You have not created a work item yet' :
              items.map((itemObj, i) => {
                return (
                  <Box key={itemObj.id}>
                    <ListItem
                    >
                      <ListItemText
                        primary={<Typography
                          sx={{ display: 'inline' }}
                          component="span"
                          variant="h6"
                          color="text.primary"
                        >
                          {itemObj.title}
                        </Typography>}
                        secondary={
                          <React.Fragment>
                            <Typography
                              sx={{ display: 'inline' }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                                  Created on: {
                                <Moment format="DD/MM/YYYY">
                                  {itemObj.createdOn}
                                </Moment>
                              }
                            </Typography>
                            <Button sx={{ display: 'block' }}
                              onClick={() => handleDetailsClick(itemObj.id)}
                            >
                              View details
                            </Button>
                          </React.Fragment>
                        }>
                      </ListItemText>
                      <Chip label={itemObj.status} color="primary" sx={{ color: 'white' }} />
                    </ListItem>
                    {i < items.length - 1 ?
                        (<Divider variant="middle" component="li" />) :
                        null}
                  </Box>
                );
              })
            }
          </List>
        </TabPanel>
        <TabPanel value='1' index={1}>
          <Box className='items-per-team' sx={{ textAlign: 'left' }}>
            {
              teamItems.length === 0 ?
              'There are no team items yet' :
              teamItems.reduce((acc, el) => {
                if (!acc.includes(el.teamName)) {
                  acc.push(el.teamName);
                }

                return acc;
              }, [])
                  .map((teamName) => {
                    return (
                      <Box key={teamName} sx={{ mt: 2 }}>
                        <Typography
                          component="h6"
                          variant="h6"
                          sx={{ color: 'text.primary' }}>
                          {teamName}
                        </Typography>
                        <Box sx={{ display: 'flex', flexWrap: 'wrap', align: 'center', maxWidth: '1180px' }}>
                          {
                            teamItems.filter((item) => teamName === item.teamName)
                                .map((itemObj) => {
                                  return (
                                    <Paper key={itemObj.id}
                                      sx={{ display: 'inline-block', width: 300, mb: 3, mt: 2, mr: 4 }}>
                                      <ListItem
                                      >
                                        <ListItemText
                                          primary={<Typography
                                            sx={{ display: 'inline', fontSize: 16 }}
                                            component="span"
                                            variant="h6"
                                            color="text.primary"
                                          >
                                            {itemObj.title}
                                          </Typography>}
                                          secondary={
                                            <React.Fragment>
                                              <Typography
                                                sx={{ display: 'inline' }}
                                                component="span"
                                                variant="body2"
                                                color="text.primary"
                                              >
                                  Created on: {
                                                  <Moment format="DD/MM/YYYY">
                                                    {itemObj.createdOn}
                                                  </Moment>
                                                }
                                              </Typography>
                                              <Button sx={{ display: 'block' }}
                                                onClick={() => handleDetailsClick(itemObj.id)}
                                              >
                              View details
                                              </Button>
                                              {}
                                            </React.Fragment>
                                          }>
                                        </ListItemText>
                                        <Chip label={itemObj.status} color="primary" sx={{ color: 'white' }} />
                                      </ListItem>
                                    </Paper>
                                  );
                                })
                          }
                        </Box>
                      </Box>
                    );
                  })
            }
          </Box>

        </TabPanel>
      </TabContext>
    </Box>
  );
};

export default ViewWorkItems;
