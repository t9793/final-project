import React, { useRef } from 'react';
import Container from '@mui/material/Container';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import Button from '@mui/material/Button';
import { keyframes } from '@mui/system';
import Typography from '@mui/material/Typography';
import { Grid } from '@mui/material';
import Review from '../../images/review.png';
import Submit from '../../images/submit.png';
import Approve from '../../images/approved.png';
import LandingPageCard from '../LandingPageCard/LandingPageCard';
import { useNavigate } from 'react-router-dom';
import './LandingPage.css';
import Background from '../../images/pexels-pixabay-434337.jpg';
import TypewriterText from '../Typewritter/Typewritter';

const LandingPage = () => {
  const cardsContents = [
    [
      Submit,
      'Submit your work to the platform by writing it directly in our text editor or uploading a file',
    ],
    [
      Review,
      'Your teammates will read your work, comment on it, and leave suggestions and request changes',
    ],
    [
      Approve,
      'Once all the issues are resolved, get your work approved for publication',
    ],
  ];
  const secondSectionRef = useRef();
  const thirdSectionRef = useRef();
  const navigate = useNavigate();

  const bounce = keyframes`
  from {
    transform: translateY(-0.5rem);
  }
  to {
    transform: translateY(0.5rem);
  }
`;

  const scrollTo = (ref) => {
    ref.current.scrollIntoView();
  };

  return (
    <div className="LandingPage">
      <Container
        disableGutters={false}
        maxWidth="false"
        sx={{
          width: '100%',
          height: '93.4vh',
          backgroundImage: `url(${Background})`,
          backgroundSize: 'cover',
          backgroundPosition: 'bottom',
          position: 'relative',
        }}
      >
        <Container
          sx={{
            height: '100%',
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Typography
            variant="h2"
            component="span"
            sx={{
              fontWeight: 700,
              color: 'inherit',
              textAlign: 'left',
              mb: 5,
            }}
          >
            Collaborate with your <br/> team to create stellar <TypewriterText sx={{ display: 'inline-block' }} />
          </Typography>

        </Container>
        <Button
          className="scrollButton"
          onClick={() => scrollTo(secondSectionRef)}
          variant="text"
          size="l"
          sx={{
            position: 'absolute',
            bottom: '0',
            transform: 'translateX(-25%)',
            color: 'primary',
          }}
        >
          <KeyboardArrowDownIcon
            sx={{
              fontSize: '4rem',
              animation: `${bounce} 1s infinite alternate ease-in-out`,
            }}
          />
        </Button>
      </Container>
      <Container
        ref={secondSectionRef}
        disableGutters={false}
        maxWidth="false"
        sx={{
          backgroundColor: '#fafbfb',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          height: '100vh',
          position: 'relative',
        }}
      >
        <Typography variant="h3" component="h3" sx={{ mb: '6rem' }}>
          How does CollaboWriter work?
        </Typography>
        <Grid
          container
          spacing={0}
          columnSpacing={5}
          sx={{
            margin: '0 auto',
            maxWidth: '70%',
          }}
        >
          {cardsContents.map(([icon, text], i) => (
            <LandingPageCard key={i} icon={icon} text={text} />
          ))}
        </Grid>
        <Button
          className="scrollButton"
          onClick={() => scrollTo(thirdSectionRef)}
          variant="text"
          size="l"
          sx={{
            position: 'absolute',
            bottom: '0',
            transform: 'translateX(25%)',
            color: 'primary',
          }}
        >
          <KeyboardArrowDownIcon
            sx={{
              fontSize: '4rem',
              animation: `${bounce} 1s infinite alternate ease-in-out`,
            }}
          ></KeyboardArrowDownIcon>
        </Button>
      </Container>
      <Container
        ref={thirdSectionRef}
        disableGutters={false}
        maxWidth="false"
        sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          width: '100%',
          height: '90vh',
          backgroundColor: '#fff',
          position: 'relative',
        }}
      >
        <div>
          <Typography
            variant="h3"
            component="h3"
            sx={{
              textTransform: 'uppercase',
              fontWeight: '900',
              textAlign: 'center',
              fontSize: '5rem',
              color: '#066a73',
            }}
          >
            Sounds good?
          </Typography>
          <Typography
            variant="h6"
            component="h6"
            sx={{
              color: 'inherit',
              textAlign: 'center',
              fontWeight: '400',
              fontSize: '1.4rem',
            }}
          >
            Create your CollaboWriter account today!
          </Typography>
          <Button
            variant="contained"
            color='primary'
            disableElevation
            size="large"
            sx={{
              mt: '10rem',
            }}
            onClick={() => navigate('signup')}
          >
            Sign Up
          </Button>
        </div>
      </Container>
      <Container
        maxWidth={false}
        sx={{
          height: '10vh',
          padding: '0',
          margin: '0',
          backgroundColor: '#066a73',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Typography
          variant="body"
          component="p"
          sx={{
            fontSize: '1rem',
            color: 'white',
            textAlign: 'center',
            fontWeight: '200',
          }}
        >
          &copy; 2022 Alexander Tanev & Tsvetelina Markova | Telerik Academy
        </Typography>
      </Container>
    </div>
  );
};

export default LandingPage;
