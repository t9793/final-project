import { Box, Chip, Divider, Grid, List, ListItem, ListItemText, Paper, Typography } from '@mui/material';
import { Container } from '@mui/system';
import React from 'react';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import { useEffect } from 'react';
import { useState } from 'react';
import { getAllWorkItems, getWorkItemsByAuthor } from '../../services/work-items.services';
import Moment from 'react-moment';
import Chart from '../Chart/Chart';
import workItemStatusEnum from '../../enums/work-item-status';
import AccessTimeIcon from '@mui/icons-material/AccessTime';

const DashboardHome = () => {
  const { userData } = useContext(AppContext);
  const currentHour = new Date().getHours();
  const [workItems, setWorkItems] = useState([]);
  const [reviewItems, setReviewItems] = useState([]);
  const [reviewRequestStats, setReviewRequestStats] = useState([]);

  useEffect(() => {
    if (!userData) return;

    getWorkItemsByAuthor(userData.username)
        .then((workItems) => {
          if (workItems.length !== 0) {
            const res = workItems.sort(
                (a, b) => b.createdOn - a.createdOn).slice(0, 3);

            return res;
          } else {
            return [];
          }
        })
        .then(setWorkItems)
        .catch(console.error);

    getAllWorkItems()
        .then((workItems) => {
          if (workItems.length !== 0) {
            const res = workItems.
                filter((item) => item.reviewers[userData.username])
                .sort(
                    (a, b) => b.reviewRequest.createdOn - a.reviewRequest.createdOn);

            return res;
          } else {
            return [];
          }
        })
        .then((res) => {
          setReviewItems(res);
          setReviewRequestStats(res.reduce((acc, item) => {
            const status = item.reviewers[userData.username];

            if (status === workItemStatusEnum.Pending) {
              acc[0].value++;
            } else if (status === workItemStatusEnum.Accepted) {
              acc[1].value++;
            } else if (status === workItemStatusEnum.Rejected) {
              acc[2].value++;
            }

            return acc;
          }, [
            { name: workItemStatusEnum.Pending, value: 0 },
            { name: workItemStatusEnum.Accepted, value: 0 },
            { name: workItemStatusEnum.Rejected, value: 0 },
          ]));
        })
        .catch(console.error);
  }, [userData]);

  return (
    <Container
      className="DashboardHome"
      sx={{ textAlign: 'left' }}
    >
      <Typography variant="h4" component="h4">
        {`${currentHour >= 12 ?
        (currentHour < 18 ? 'Good afternoon' : 'Good evening') :
        'Good morning'}, ${userData.username}!`}
      </Typography>
      <Grid container rowSpacing={2} columnSpacing={{ xs: 2 }} sx={{ mt: 2 }}>
        <Grid item xs={6}>
          <Paper elevation={1} sx={{ padding: 4, borderRadius: 5, height: 330 }}>
            <Typography variant="h6" component="h6">
              Your Latest Work Items
            </Typography>
            <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
              {workItems.length !== 0 ? workItems.map((item, i) => {
                return (
                  <Box key={item.id}>
                    <ListItem
                      alignItems="flex-start"
                      secondaryAction={
                        <Chip label={item.status} color="primary" sx={{ color: 'white' }} />
                      }>
                      <ListItemText
                        primary={<Typography
                          sx={{ display: 'block', fontSize: 16 }}
                          component="span"
                          variant="h6"
                          color="text.primary"
                        >
                          {item.title}
                        </Typography>}
                        secondary={
                          <Typography
                            sx={{ display: 'block', fontSize: 12 }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            <AccessTimeIcon sx={{ height: '14px' }} /> <Moment fromNow>{item.createdOn}</Moment>
                          </Typography>
                        }
                      />
                    </ListItem>
                    {i < (workItems.length - 1) ?
                      (<Divider variant="middle" component="li" />) :
                      null}
                  </Box>
                );
              }) : <Typography
                sx={{ textAlign: 'center', mt: 12 }}
                variant="body"
                component="div">
                You have not added any work items yet.
              </Typography>}
            </List>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper elevation={1} sx={{ padding: 4, borderRadius: 5, height: 330 }}>
            <Typography variant="h6" component="h6">
              Latest Work Items for Review
            </Typography>
            <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
              {reviewItems.length !== 0 ? reviewItems.slice(0, 3).map((item, i) => {
                return (
                  <Box key={item.id}>
                    <ListItem
                      alignItems="flex-start"
                      secondaryAction={
                        <Chip label={item.status} color="primary" sx={{ color: 'white' }} />
                      }>
                      <ListItemText
                        primary={<Typography
                          sx={{ display: 'block', fontSize: 16 }}
                          component="span"
                          variant="h6"
                          color="text.primary"
                        >
                          {item.title}
                        </Typography>}
                        secondary={
                          <Typography
                            sx={{ display: 'block', fontSize: 12 }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            <AccessTimeIcon sx={{ height: '14px' }} /> <Moment fromNow>{item.createdOn}</Moment>
                          </Typography>
                        }
                      />
                    </ListItem>
                    {(i < (reviewItems.length - 1) && i < 2 )?
                      (<Divider variant="middle" component="li" />) :
                      null}
                  </Box>
                );
              }) : <Typography
                sx={{ textAlign: 'center', mt: 12 }}
                variant="body"
                component="div">
                You do not have any work items to review.
              </Typography>}
            </List>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper elevation={1} sx={{ padding: 4, borderRadius: 5, height: 330 }}>
            <Typography variant="h6" component="h6">
              Review Request Statistics
            </Typography>
            <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              <Chart data={reviewRequestStats} />
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper elevation={1} sx={{ padding: 4, borderRadius: 5, height: 330 }}>
            <Typography variant="h6" component="h6">
              Your Teams
            </Typography>
            <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
              {userData.teams ? Object.entries(userData.teams).slice(0, 5).map(([teamId, teamName], i) => {
                return (
                  <Box key={teamId}>
                    <ListItem
                      alignItems="flex-start">
                      <ListItemText
                        primary={teamName}
                      />
                    </ListItem>
                    {i < (Object.keys(userData.teams).length - 1) && i < 4 ?
                      (<Divider variant="middle" component="li" />) :
                      null}
                  </Box>
                );
              }) : <Typography
                sx={{ textAlign: 'center', mt: 12 }}
                variant="body"
                component="div">
                You do not belong to any teams yet.
              </Typography>}
            </List>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
};

export default DashboardHome;
