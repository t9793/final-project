import React, { useState, useContext } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {
  addHistoryEntry,
  updateReviewerStatus,
  updateWorkItemStatus,
  getWorkItemById,
} from '../../services/work-items.services';
import { createNotification } from '../../services/notifications.services';
import notificationType from '../../enums/notification-type';
import workItemStatusEnum from '../../enums/work-item-status';
import AppContext from '../../providers/AppContext';

const CommentDialog = ({
  open,
  setOpen,
  dialogText,
  dialogTitle,
  id,
  username,
  newStatus,
}) => {
  const [comment, setComment] = useState('');
  const { userData } = useContext(AppContext);

  const handleSubmit = () => {
    addHistoryEntry(id, username, comment, newStatus).catch(console.error);
    if (newStatus === workItemStatusEnum.Rejected) {
      updateReviewerStatus(id, newStatus, username)
          .catch(console.error);
    }
    setOpen(false);

    if (newStatus !== '') {
      updateWorkItemStatus(id, newStatus).catch(console.error);
    }
    getWorkItemById(id)
        .then((item) => {
          const fromObj = {
            [id]: item.title,
          };
          const message = `${userData.username} commented on work item: "${item.title}"! Click to view.`;
          const recipientsArr = [...Object.keys(item.reviewers), item.author]
              .filter((username) => username !== userData.username);

          recipientsArr.map( (username) => {
            return createNotification(
                notificationType.comment,
                username,
                fromObj,
                message,
                Date.now())
                .catch(console.error);
          },

          );
        })
        .catch(console.error);
  };

  return (
    <div>
      <Dialog open={open} onClose={() => setOpen(false)} fullWidth>
        <DialogTitle>{dialogTitle}</DialogTitle>
        <DialogContent>
          <DialogContentText>{dialogText}</DialogContentText>
          <TextField
            onChange={(e) => {
              setComment(e.target.value);
            }}
            multiline
            rows="5"
            variant="outlined"
            autoFocus
            id="comment"
            label="Comment"
            fullWidth
          />
        </DialogContent>
        <DialogActions sx={{ mb: '1rem', mr: '1rem' }}>
          <Button onClick={() => setOpen(false)}>Cancel</Button>
          <Button variant="contained" onClick={handleSubmit}>
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default CommentDialog;
