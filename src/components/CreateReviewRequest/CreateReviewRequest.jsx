import { AttachFile, Group } from '@mui/icons-material';
import {
  Box,
  Button,
  Chip,
  Container,
  FormControl,
  FormHelperText,
  Input,
  TextField,
  Typography,
} from '@mui/material';
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from 'firebase/storage';
import { storage } from '../../config/firebase-config';
import React, { useState } from 'react';
import TextEditor from '../TextEditor/TextEditor';
import { v4 } from 'uuid';
import ReviewersDialog from '../ReviewersDialog/ReviewersDialog';
import {
  addAttachmentToReviewRequest,
  addReviewRequest,
  getLiveWorkItemData,
  getWorkItemById,
  updateReviewers,
} from '../../services/work-items.services';
import { useParams } from 'react-router-dom';
import { createNotification } from '../../services/notifications.services';
import notificationType from '../../enums/notification-type';
import { useEffect } from 'react';
import { getTeamById } from '../../services/teams.services';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import { useNavigate } from 'react-router-dom';

export default function CreateReviewRequest() {
  const { id } = useParams();
  const { userData } = useContext(AppContext);
  const [workItem, setWorkItem] = useState({});
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [titleError, setTitleError] = useState(false);
  const [descriptionError, setDescriptionError] = useState(false);
  const [reviewers, setReviewers] = useState([]);
  // const [reviewersError, setReviewersError] = useState(false);
  const [attachmentUpload, setAttachmentUpload] = useState({});
  const [open, setOpen] = useState(false);
  const [selectedValues, setSelectedValues] = useState({});
  const [optionsArray, setOptionsArray] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const unsubscribe = getLiveWorkItemData(id, () => {
      getWorkItemById(id).then(setWorkItem).catch(console.error);
    });

    return () => unsubscribe();
  }, [id]);

  useEffect(() => {
    if (workItem.teamId) {
      getTeamById(workItem.teamId)
          .then((teamData) => {
            setOptionsArray(Object
                .entries(teamData.members)
                .filter(([key, value]) =>
                  value !== 'invited' &&
                key !== userData.username)
                .reduce((acc, [key]) => {
                  acc.push(key);

                  return acc;
                }, []));
          })
          .catch(console.error);
    }
  }, [workItem]);

  useEffect(() => {
    if (workItem.reviewers) {
      setReviewers(Object.keys(workItem.reviewers));
      setSelectedValues(workItem.reviewers);
    }
  }, [workItem]);

  const isInputValid = () => {
    let isValid = true;

    if (title.length <= 10 || title.length >= 80) {
      setTitleError(true);
      isValid = false;
    }

    if (description.length < 20) {
      setDescriptionError(true);
      isValid = false;
    }

    if (reviewers.length === 0) {
      isValid = false;
    }

    return isValid;
  };

  const handleAttachmentSelect = (e) => {
    const attachments = Array.from(e.target.files).reduce(
        (acc, file) => {
          const attachmentId = v4();
          acc[attachmentId] = file;

          return acc;
        }, {},
    );

    setAttachmentUpload((prev) =>( { ...prev, ...attachments }));
  };

  const uploadAttachment = (file, workItemId, attachmentId) => {
    if (!file) return;

    const attachmentFile = storageRef(
        storage,
        `workItems/${workItemId}/reviewRequest/attachments/${attachmentId}`,
    );

    uploadBytes(attachmentFile, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            return addAttachmentToReviewRequest(
                id,
                attachmentId,
                url,
                file.name,
            );
          });
        })
        .catch(console.error);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    setSelectedValues({ ...value });
    const res = Object.entries(value).reduce((acc, [key, val]) => {
      if (val !== false) {
        acc.push(key);
      }

      return acc;
    }, []);

    setReviewers(res);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setTitleError(false);
    setDescriptionError(false);
    // setReviewersError(false);

    if (isInputValid() === true) {
      addReviewRequest(id, title, description)
          .then((item) => {
            if (Object.keys(attachmentUpload).length !== 0) {
              Object.entries(attachmentUpload).forEach(([key, file]) =>
                uploadAttachment(file, item.id, key),
              );
            }
          })
          .then(() => {
            const fromObj = {
              [id]: title,
            };
            const message = 'You\'ve been asked to review a work item! ' +
            'Go to "My review requests" to see the request.';
            reviewers.map( (username) => {
              return createNotification(
                  notificationType.reviewRequest,
                  username,
                  fromObj,
                  message,
                  Date.now(),
              );
            });
          })
          .then(() => updateReviewers(id, reviewers))
          .then(() => navigate(-1))
          .catch(console.error);
    }
  };

  return (
    <Container
      className="SubmitWorkItemForm"
      sx={{ m: '5', position: 'relative' }}
    >
      <Typography
        variant="h4"
        component="h4"
        sx={{
          mt: '2rem',
          mb: '1rem',
          fontWeight: 400,
          color: 'inherit',
          textAlign: 'left',
        }}
      >
        Create Review Request
      </Typography>
      <form noValidate autoComplete="off">
        <Box mb={2}>
          <TextField
            sx={{ bgcolor: 'white' }}
            onChange={(e) => setTitle(e.target.value)}
            label="Title"
            variant="outlined"
            fullWidth
            required
            error={titleError}
            helperText={
              titleError ?
                'The title should be between 10 and 80 characters' :
                ''
            }
          />
        </Box>
        <FormControl
          error={descriptionError}
          sx={{ width: '100%', height: 300 }}
        >
          <TextEditor setDescription={setDescription} />
          <FormHelperText>
            {descriptionError ?
              'The description should be at least 20 characters long' :
              ''}
          </FormHelperText>
        </FormControl>
        <Box sx={{ textAlign: 'left' }}>
          <label htmlFor="button-attachments">
            <Input
              sx={{ display: 'none' }}
              id="button-attachments"
              type="file"
              inputProps={{ multiple: true }}
              onChange={(e) => handleAttachmentSelect(e)}
            />
            <Button
              variant="text"
              component="span"
              disableElevation
              startIcon={<AttachFile />}
            >
              select attachments
            </Button>
          </label>
          {Object.keys(attachmentUpload).length !== 0 ?
            Object.entries(attachmentUpload).map(([key, file]) => {
              return (
                <Chip
                  key={key}
                  color="primary"
                  variant="outlined"
                  sx={{ ml: 2 }}
                  label={file.name}
                  onDelete={() => {
                    const attachments = { ...attachmentUpload };
                    delete attachments[key];
                    setAttachmentUpload(attachments);
                  }}
                />
              );
            }) :
            null}
        </Box>
        <Box sx={{ display: 'flex', alignItems: 'center', m: 0, p: 0 }}>
          <Button
            variant="text"
            component="span"
            disabled={optionsArray.length === 0 ? true : false}
            disableElevation
            startIcon={<Group />}
            onClick={handleClickOpen}
          >
            Add/remove reviewers
          </Button>
          <Typography variant="body1" component="span" sx={{ ml: '2rem' }}>
            {reviewers.length ?
              'Currently assigned: ' + reviewers.join(', ') :
              ''}
          </Typography>
          <ReviewersDialog
            selectedValues={selectedValues}
            open={open}
            onClose={handleClose}
            optionsArray={optionsArray}
          />
        </Box>
        <Box sx={{ display: 'flex', alignItems: 'flex-start' }}>
          <Button
            sx={{ color: 'white' }}
            onClick={(e) => handleSubmit(e)}
            type="submit"
            color="primary"
            variant="contained"
            size="large"
            disableElevation
          >
            Submit
          </Button>
          <Button
            type="text"
            color="primary"
            variant="outlined"
            size="large"
            sx={{ ml: '1rem' }}
          >
            Cancel
          </Button>
        </Box>
      </form>
    </Container>
  );
}
