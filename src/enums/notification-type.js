const notificationType = {

  invitation: 'invitation',
  statusUpdate: 'statusUpdate',
  reviewRequest: 'reviewRequest',
  comment: 'comment',
};

export default notificationType;
