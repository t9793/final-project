import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyDgQg9zh20fWJFoPuyTn-CUoxIjuEK86w4',
  authDomain: 'final-project-40ce1.firebaseapp.com',
  databaseURL: 'https://final-project-40ce1-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'final-project-40ce1',
  storageBucket: 'final-project-40ce1.appspot.com',
  messagingSenderId: '721159276832',
  appId: '1:721159276832:web:1ad5c24cf9dde260c9ef45',
};


const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
