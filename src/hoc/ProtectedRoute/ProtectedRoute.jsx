import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import LoadingAnimation from '../../components/LoadingAnimation/LoadingAnimation';

const ProtectedRoute = ({ loading, user, error, children }) => {
  const location = useLocation();

  if (loading) {
    return <LoadingAnimation />;
  }

  if (!user || error) {
    return <Navigate to="/" state={{ from: location }} replace />;
  }

  return children;
};

export default ProtectedRoute;
