# CollaboWriter - a writer's app for collaboration and work evaluation 

CollaboWriter is a fast and easy to use SPA for creating, and sharing your work as a writer, poet, lyricist. It facilitates collaboration by working in teams for reviewing and improving work items.

## How to Run

Install all dependencies and then start the app.

```
npm i
npm start
```

## Core Components and Features

### Landing page

Landing page is accessible only for users who have not signed into a registered account. It aims to display the basic concepts of the application and provide a Sign up/Log in forms.

Upon successful registration and log in the user will be automatically redirected to the Dashboard view.


### Dashboard

This is where the core functionalities are present, and available only for registered users.

Users are presented with a new navigation bar that has a Notifications and Profile components, as well as an expandable drawer on the left part of the screen which holds the list of key functionalities of the application. 

Please note, upon initial login, users are restricted from accessing Review Requests/Work Items parts of the app, as they would first need to create or join a team in order to use them. This is part of the main idea behind the app - it is only meant for collaboration and not "Single player" mode.

### Profile

By clicking on the profile icon in the top right corner, you can select log out, or Profile, where you will be able to edit your profile info and select/update your profile pic.

### Teams/Create team

In order to create your first team and start creating work items -> click on Teams icon on the left menu, then Create team.

Make sure to give a unique team name, 

The search field will give you suggestions as you type, and will list your selections with their selected avatar. (You cannot select yourself or an user more than once.)

### Team/My team

Gives an overview of your teams, on expand you can see the team members, their status, their profile pic. You can also leave the team or invite others to it.

### Work Items/Create work item

Once you've created or joined a team, you can create work items. 

Work items are pieces of well ... work. It can be a passage or chapter, or even just a few lines you were inspired to write today. You have a few options: to upload a doc/pdf file OR write it down in the text editor field. If you choose to upload -> you can then read your file directly in the browser.

In addition you can also upload files, anything you feel is complementary to the item.

### Work Items/View work items

By clicking through the tabs you are presented with lists of items you've created, you are asked to review or you can view all work items in any of your teams.

### Review Requests/My Review Requests

You can see the review requests you've created, and by clicking on Details you'll be redirected to the review request and work item page.

### Work Item Details

By clicking on view details you can see the work item you've created.
An option to comment on a teammate's work item and if there exists a corresponding work review request for this item, an assigned reviewer will be able to see the status buttons. If every reviewer accepts the work item, it is considered accepted.

### Review Requests 

For each work item, an author can submit a review request - by assigning reviewers, leaving a reviewer status to 'Pending' by default, a title and description. Attachments can also be uploaded if necessary for hte reviewers.

## Authors
Created by Alexander Tanev and Tsvetelina Markova

*June 2022*